let main = () => {
  document.getElementById("v-pills-bai-7-tab").click();
  let bai_7 = () => {
    let i = 2,
      j,
      flag = 1;
    let n =
      parseInt(document.querySelectorAll(".bai-tap-7 .inputN")[0].value) || 0;
    let output = "";
    if (n < 2) {
      output = "Số bạn nhập vào không hợp lệ";
    } else if (n == 2) {
      output = "2";
    } else {
      for (i = 2; i <= n; i++) {
        flag = 1;
        for (j = 2; j <= Math.sqrt(i); j++) {
          if (!(i % j)) {
            flag = 0;
            break;
          }
        }

        flag && (output += `${i} `);
      }
    }
    document.querySelectorAll(".txt-result-bai-7")[0].innerHTML = output;
  };
  // giai bai 7
  document
    .querySelectorAll(".btn-calc-bai-7")[0]
    .addEventListener("click", (e) => {
      e.preventDefault();
      bai_7();
    });
};

main();
